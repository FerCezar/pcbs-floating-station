# PCB Floating Station

Water quality monitoring board based on ESP32 and LoRaWAN.

- Power (9V~25V)
    - Solar Panel
    - Bateries
    - DC Power Supply

- Communication Interfaces:
    - LoRaWAN
    - SD Card
    - RS485
    - I2C
    - UART
    - 4-20mA Analog
    - 0~5V Analog
    - GPIO 

- Compatible Sensors:
    - Athlas Scientific (ENV20 and ENV50)
    - Any other with compatible communication interface


```mermaid
flowchart TD
    A(ESP32) <--> B(RS485)
    A --> C(LoRaWAN)
    A <--> D(SD Card)
    E(0~5V Analog) --> A
    F(4~20mA Analog) --> A
    G(GPIO) <--> A
    H(I2C) <--> A
    A <--> I(UART) 

```
  

## REV 0.1
![v1](PCB-FST-V1.png)

## REV 0.2
![v1](PCB-FST-V1-REV0.2.png)

Board developed to UTFPR and SANEPAR.
